#include <SoftwareSerial.h>

#define BLUETOOTH_RX_PIN 8
#define BLUETOOTH_TX_PIN 7 
#define BLUETOOTH_BOUD 9600
// If the baudrate of the HM-10 module has been updated,
// you may need to change 9600 by another value
// Once you have found the correct baudrate,
// you can update it using AT+BAUDx command 
// e.g. AT+BAUD0 for 9600 bauds

#define SERIAL_BOUD 115200

#define LED_PIN 9

#define TOGGLE 3

#define ERR -1

SoftwareSerial Bluetooth(BLUETOOTH_RX_PIN, BLUETOOTH_TX_PIN); // RX, TX  
// Connection
// HM10        Arduino Uno
// Pin 1/TXD   Pin 7
// Pin 2/RXD   Pin 8

void setup() {  
  Serial.begin(SERIAL_BOUD);
  while(!Serial) { }
  Bluetooth.begin(BLUETOOTH_BOUD);

  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  // Ignoring Bluetooth RX, TX pins
  // pinMode(7, OUTPUT);
  // pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);

  digitalWrite(9,  HIGH);
  digitalWrite(10, HIGH);
  digitalWrite(11, HIGH);
  digitalWrite(12, HIGH);
  digitalWrite(13, HIGH);
}

void loop() {  
  if (Bluetooth.available()) {
    // Loop bluetooth serial log
    // Serial.write(Bluetooth.read());  

    char cardinal = Bluetooth.read();
    processCardinal(cardinal);
  }
}

// Assuming a simple cardinal input 
// (S)top, (F)orward, (R)right, (L)eft, (H)onk
void processCardinal(char step) {
  if (step == 'S') {
    digitalWrite(9, HIGH);
    digitalWrite(10, HIGH);
  } else if (step == 'F') {
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
  } else if (step == 'R') {
    digitalWrite(9, HIGH);
    digitalWrite(10, LOW);
  } else if (step == 'L') {
    digitalWrite(9, LOW);
    digitalWrite(10, HIGH);
  } else if (step == 'H') {
    digitalWrite(13, !digitalRead(13));
  }
}
